//
//  LoginView.swift
//  ThingsTodo
//
//  Created by Muhammad Ali Maniar on 26/05/2021.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBox: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func googleTapped(_ sender: UIButton) {
        
    }

}
