//
//  TodoDetailViewModel.swift
//  ThingsTodo
//
//  Created by Muhammad Ali Maniar on 26/05/2021.
//

import Foundation

protocol TodoDetailsViewModelInput {
    
}

protocol TodoDetailsViewModelOutput {

}

protocol TodoDetailViewModel: TodoDetailsViewModelInput, TodoDetailsViewModelOutput { }


final class DefaultTodoDetailViewModel: TodoDetailViewModel {
    
    init(todo: Todo) {
        
    }
    
}
