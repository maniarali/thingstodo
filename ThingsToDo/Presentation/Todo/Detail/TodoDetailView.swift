//
//  TodoDetailView.swift
//  ThingsTodo
//
//  Created by Muhammad Ali Maniar on 26/05/2021.
//

import UIKit

class TodoDetailView: UIViewController, StoryboardInstantiable {

    // MARK: - Lifecycle

    private var viewModel: TodoDetailViewModel!
    
    static func create(with viewModel: TodoDetailViewModel) -> TodoDetailView {
        let view = TodoDetailView.instantiateViewController()
        view.viewModel = viewModel
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
