//
//  TodoListView.swift
//  ThingsTodo
//
//  Created by Muhammad Ali Maniar on 26/05/2021.
//

import UIKit

class TodoListView: UIViewController, StoryboardInstantiable {

    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel: TodoListViewModel!
    
    // MARK: - Lifecycle
    static func create(with viewModel: TodoListViewModel) -> TodoListView {
        let view = TodoListView.instantiateViewController()
        view.viewModel = viewModel
        
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableView()
        setupViews()
        bind(to: viewModel)
    }
    
    // MARK: - Private
    private func setTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    private func setupViews() {
        title = viewModel.screenTitle
    }
    
    private func bind(to viewModel: TodoListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.udpateTable() }
        viewModel.loading.observe(on: self) { [weak self] (isLoading) in self?.updateLoading(isLoading) }
    }
    
    private func udpateTable() {
        tableView.reloadData()
    }
    
    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }

}
extension TodoListView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TodoListItemCell.reuseIdentifier,
                                                       for: indexPath) as? TodoListItemCell else {
            assertionFailure("Cannot dequeue reusable cell \(TodoListItemCell.self) with reuseIdentifier: \(TodoListItemCell.reuseIdentifier)")
            return UITableViewCell()
        }
        cell.fill(with: viewModel.items.value[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let addAction = UIContextualAction(style: .normal, title: NSLocalizedString("Done", comment: "")) { [weak self] _,_,_  in
            
            self?.viewModel.updateTodo(indexPath.row)
        }
        addAction.backgroundColor = UIColor(named: "Done")
        return UISwipeActionsConfiguration(actions: [addAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let removeAction = UIContextualAction(style: .normal, title: NSLocalizedString("Remove", comment: "")) { [weak self] _,_,_  in
            
            self?.viewModel.deleteTodo(indexPath.row)
        }
        removeAction.backgroundColor = UIColor(named: "Remove")
        return UISwipeActionsConfiguration(actions: [removeAction])
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        viewModel.didSelectItem(at: indexPath.row)
//    }
    
}
