//
//  TodoListItemCell.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import UIKit

class TodoListItemCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var amOrPmLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var completedStar: UIButton!
    @IBOutlet weak var priorityView: UIView!
    
    static let reuseIdentifier = String(describing: TodoListItemCell.self)
    
    private var viewModel: TodoListItemViewModel!
    
    func fill(with viewModel: TodoListItemViewModel) {
        self.viewModel = viewModel
        
        titleLabel.text = viewModel.title
        amOrPmLabel.text = viewModel.amOrPm
        timeLabel.text = viewModel.time
        categoryLabel.text = viewModel.category
        completedStar.isSelected = viewModel.isCompleted
        priorityView.backgroundColor = UIColor(named: viewModel.colorForPriority)
    }

}
