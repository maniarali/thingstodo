//
//  TodoListViewModel.swift
//  ThingsTodo
//
//  Created by Muhammad Ali Maniar on 26/05/2021.
//

import Foundation

struct TodoListViewModelActions {
    
    let showTodoDetails: (Todo) -> Void
}

protocol TodoListViewModelInput {
    func updateTodo(_ index: Int)
    func deleteTodo(_ index: Int)
}

protocol TodoListViewModelOutput {
    var items: Observable<[TodoListItemViewModel]> { get }
    var loading: Observable<Bool> { get }
    var screenTitle: String { get }
}

protocol TodoListViewModel:  TodoListViewModelInput, TodoListViewModelOutput { }

final class DefaultTodoListViewModel: TodoListViewModel {
    
    private let todosUseCase: TodosUseCase
    private let actions: TodoListViewModelActions?
    
    // MARK: - OUTPUT
    let items: Observable<[TodoListItemViewModel]> = Observable([])
    let loading: Observable<Bool> = Observable(false)
    
    let screenTitle = NSLocalizedString("ThingsTODO", comment: "")
    private var todoLoadTask: Cancellable? { willSet { todoLoadTask?.cancel() } }
    
    // MARK: - Init
    init(todosUseCase: TodosUseCase,
         actions: TodoListViewModelActions? = nil) {
        self.todosUseCase = todosUseCase
        self.actions = actions
        self.load()
    }
    
    private func load() {
        self.loading.value = true
        todoLoadTask = todosUseCase.execute(cached: { todos in
            self.loading.value = false
            self.mutateItemValues(todos)
        }, completion: { (result) in
            
            self.loading.value = false
            switch result {
            case .success(let todos):
                self.mutateItemValues(todos)
            case .failure(let error):
                print("ERROR \(error)")
            }
        })
    }
    
    private func mutateItemValues(_ todos: [Todo]) {
        self.items.value = todos.map({ (todo) -> TodoListItemViewModel in
            return TodoListItemViewModel(todo: todo)
        })
    }
    
    func deleteTodo(_ index: Int) {
        
        self.loading.value = true
        let _ = todosUseCase.update(id: self.items.value[index].id,
                                    todo: getUpdateTodoParams() ) { (result) in
            
            self.loading.value = false
            switch result {
            case .success(_):
                self.items.value.remove(at: index)
            case .failure(let error):
                print("ERROR \(error)")
            }
        }
    }
    func updateTodo(_ index: Int) {
        
        self.loading.value = true
        let _ = todosUseCase.update(id: self.items.value[index].id,
                                    todo: getUpdateTodoParams() ) { (result) in
            
            self.loading.value = false
            switch result {
            case .success(let todo):
                self.items.value[index] = TodoListItemViewModel(todo: todo)
            case .failure(let error):
                print("ERROR \(error)")
            }
        }
    }
    
    private func getUpdateTodoParams() -> [String: Any] {
        return ["isCompleted" : true]
    }
}
