//
//  TodoListItemViewModel.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import Foundation

struct TodoListItemViewModel: Equatable {
    let id: String
    let title: String
    let category: String
    let isCompleted: Bool
    let time: String
    let amOrPm: String
    private let priority: Todo.Priority
    var colorForPriority: String {
        switch priority {
        case .High:
            return "Red"
        case .Medium:
            return "Orange"
        case .Normal:
            return "Blue"
        case .Low:
            return "Green"
        }
    }
    
}

extension TodoListItemViewModel {
    init(todo: Todo) {
        self.id = todo.id
        self.title = todo.title
        self.category = todo.category
        self.isCompleted = todo.isCompleted
        let date = Date(timeIntervalSince1970: Double(todo.timestamp))
        self.time = timeFormatter.string(from: date)
        self.amOrPm = meridiesFormatter.string(from: date)
        self.priority = todo.priority
    }
}

fileprivate let timeFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.locale = Locale.current
    formatter.dateFormat = "hh:mm"
    return formatter
}()

fileprivate let meridiesFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "a"
    return formatter
}()
