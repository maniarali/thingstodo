//
//  TodoListFlowCoordinator.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import UIKit

protocol TodoListFlowCoordinatorDependencies {
    func makeTodoListViewController(actions: TodoListViewModelActions) -> TodoListView
    func makeTodoDetailsViewController(todo: Todo) -> UIViewController
}

final class TodoListFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: TodoListFlowCoordinatorDependencies
    
    
    private weak var todoListVC: TodoListView?
    
    init(navigationController: UINavigationController,
         dependencies: TodoListFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        // Note: here we keep strong reference with actions, this way this flow do not need to be strong referenced
        let actions = TodoListViewModelActions(showTodoDetails: showTodoDetails)
        let vc = dependencies.makeTodoListViewController(actions: actions)
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    private func showTodoDetails(todo: Todo) {
        let vc = dependencies.makeTodoDetailsViewController(todo: todo)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
