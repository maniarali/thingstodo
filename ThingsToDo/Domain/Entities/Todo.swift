//
//  TodoItem.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import Foundation

struct Todo: Decodable, Equatable, Identifiable {
    
    let id: String
    let title: String
    let description: String
    let category: String
    let timestamp: Int
    let priority: Priority
    let user_id: String
    let isCompleted: Bool
    
    enum Priority: String, Decodable {
        case High
        case Medium
        case Normal
        case Low
    }
}
