//
//  TodoUseCase.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import Foundation

protocol TodosUseCase {
    func execute(cached: @escaping ([Todo]) -> Void,
                 completion: @escaping (Result<[Todo], Error>) -> Void) -> Cancellable?
    func update(id: String, todo: [String:Any], completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable?
    func delete(id: String, completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable?
}

final class DefaultTodoUseCase: TodosUseCase {
    private let todoRepository: TodosRepository
    
    init(todoRepository: TodosRepository) {

        self.todoRepository = todoRepository
    }
    
    func execute(cached: @escaping ([Todo]) -> Void,
                 completion: @escaping (Result<[Todo], Error>) -> Void) -> Cancellable? {
        
        return todoRepository.fetchTodosList(cached: cached, completion: completion)
    }
    
    func update(id: String, todo: [String:Any], completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable? {
        
        return todoRepository.updateTodo(id: id, todo: todo, completion: completion)
    }
    
    func delete(id: String, completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable? {
        
        return todoRepository.deleteTodo(id: id, completion: completion)
    }
}
