//
//  UseCase.swift
//  ExampleMVVM
//
//  Created by Muhammad Ali Maniar on 29/05/2021.
//

import Foundation

public protocol UseCase {
    @discardableResult
    func start() -> Cancellable?
}
