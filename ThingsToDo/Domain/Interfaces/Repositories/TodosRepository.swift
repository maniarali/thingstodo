//
//  TodoRepository.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import Foundation

protocol TodosRepository {
    @discardableResult
    func fetchTodosList(cached: @escaping ([Todo]) -> Void,
                       completion: @escaping (Result<[Todo], Error>) -> Void) -> Cancellable?
    
    @discardableResult
    func updateTodo(id: String, todo: [String:Any], completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable?
    
    
    @discardableResult
    func deleteTodo(id: String, completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable?
}
