//
//  DefaultTodosRepository.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import Foundation

final class DefaultTodosRepository {
    
    private let dataTransferService: DataTransferService
    private let cache: TodosResponseStorage
    
    init(dataTransferService: DataTransferService, cache: TodosResponseStorage) {
        self.dataTransferService = dataTransferService
        self.cache = cache
    }
    
}

extension DefaultTodosRepository: TodosRepository {
    
    func fetchTodosList(cached: @escaping ([Todo]) -> Void, completion: @escaping (Result<[Todo], Error>) -> Void) -> Cancellable? {
        let task = RepositoryTask()
        
        /*
        cache.getResponse() { result in

            if case let .success(responseDTO?) = result {
                cached(responseDTO)
            }
            guard !task.isCancelled else { return }
*/
            let endpoint = APIEndpoints.getTodos()
            task.networkTask = self.dataTransferService.request(with: endpoint, completion: { result in
                switch result {
                case .success(let responseDTO):
                    self.cache.save(response: responseDTO)
                    completion(.success(responseDTO))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
//        }
        return task
    }
    
    func updateTodo(id: String, todo: [String:Any], completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable? {
        let task = RepositoryTask()
        let endpoint = APIEndpoints.updateTodo(id: id, todo: todo)
        
        task.networkTask = self.dataTransferService.request(with: endpoint, completion: { result in
            switch result {
            case .success(let responseDTO):
                completion(.success(responseDTO))
            case .failure(let error):
                completion(.failure(error))
            }
        })
        return task
    }
    
    func deleteTodo(id: String, completion: @escaping (Result<Todo, Error>) -> Void) -> Cancellable? {
        let task = RepositoryTask()
        let endpoint = APIEndpoints.deleteTodo(id: id)
        
        task.networkTask = self.dataTransferService.request(with: endpoint, completion: { result in
            switch result {
            case .success(let responseDTO):
                completion(.success(responseDTO))
            case .failure(let error):
                completion(.failure(error))
            }
        })
        return task
    }
}
