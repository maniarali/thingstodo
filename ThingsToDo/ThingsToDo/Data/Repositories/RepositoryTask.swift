//
//  RepositoryTask.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import Foundation

class RepositoryTask: Cancellable {
    var networkTask: NetworkCancellable?
    var isCancelled: Bool = false
    
    func cancel() {
        networkTask?.cancel()
        isCancelled = true
    }
}
