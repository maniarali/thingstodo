//
//  TodoResponseStorage.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import Foundation

protocol TodosResponseStorage {
    func getResponse( completion: @escaping (Result<[Todo]?, CoreDataStorageError>) -> Void)
    func save(response: [Todo])
}
