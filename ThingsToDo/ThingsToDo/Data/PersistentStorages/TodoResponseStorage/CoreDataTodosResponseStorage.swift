//
//  CoreDataTodosResponseStorage.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//
import Foundation
import CoreData

final class CoreDataTodosResponseStorage {

    private let coreDataStorage: CoreDataStorage

    init(coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.coreDataStorage = coreDataStorage
    }
}
extension CoreDataTodosResponseStorage : TodosResponseStorage {
    
    func getResponse(completion: @escaping (Result<[Todo]?, CoreDataStorageError>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            completion(.success([Todo]()))
        }
    }

    func save(response requestDto: [Todo]) {
        coreDataStorage.performBackgroundTask { context in
            
        }
    }
    
    
}
