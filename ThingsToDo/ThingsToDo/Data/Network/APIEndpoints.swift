//
//  APIEndpoints.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 31/05/2021.
//

import Foundation

struct APIEndpoints {
    
    static func getTodos() -> Endpoint<[Todo]> {

        return Endpoint(path: "todos",
                        method: .get)
    }
    
    static func updateTodo(id: String, todo: [String:Any]) -> Endpoint<Todo> {

        return Endpoint(path: "todos/\(id)",
                        method: .put,
                        headerParamaters: ["Content-Type": "application/json"],
                        bodyParamaters: todo)
    }
    
    static func deleteTodo(id: String) -> Endpoint<Todo> {

        return Endpoint(path: "todos/\(id)",
                        method: .delete)
    }
}
