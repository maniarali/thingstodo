//
//  AppConfiguration.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import Foundation

final class AppConfiguration {
    
    lazy var apiBaseURL: String = {
        return "https://6087dddba6f4a30017425143.mockapi.io/api"
    }()
}
