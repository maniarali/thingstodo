//
//  DIContainer.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import Foundation

final class AppDIContainer {
    
    lazy var appConfiguration = AppConfiguration()
    
    // MARK: - Network
    lazy var apiDataTransferService: DataTransferService = {
        let config = ApiDataNetworkConfig(baseURL: URL(string: appConfiguration.apiBaseURL)!,
                                          queryParameters: [:])
        
        let apiDataNetwork = DefaultNetworkService(config: config)
        return DefaultDataTransferService(with: apiDataNetwork)
    }()
    
    // MARK: - DIContainers of scenes
    func makeTodoSceneDIContainer() -> TodoSceneDIContainer {
        let dependencies = TodoSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService)
        return TodoSceneDIContainer(dependencies: dependencies)
    }
    
}
