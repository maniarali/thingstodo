//
//  TodoSceneDIContainer.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import UIKit

final class TodoSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: DataTransferService
    }
    
    private let dependencies: Dependencies
    
    // MARK: - Persistent Storage
    lazy var todosResponseCache: TodosResponseStorage = CoreDataTodosResponseStorage()
    
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - Use Cases
    func makeTodosUseCase() -> TodosUseCase {
        return DefaultTodoUseCase(todoRepository: makeTodosRepository())
    }
    
    // MARK: - Repositories
    func makeTodosRepository() -> TodosRepository {
        return DefaultTodosRepository(dataTransferService: dependencies.apiDataTransferService, cache: todosResponseCache)
    }
    
    // MARK: - Flow Coordinators
    func makeTodoListFlowCoordinator(navigationController: UINavigationController) -> TodoListFlowCoordinator {
        return TodoListFlowCoordinator(navigationController: navigationController,
                                           dependencies: self)
    }
}
extension TodoSceneDIContainer: TodoListFlowCoordinatorDependencies {
    
    // MARK: - Todo List
    func makeTodoListViewController(actions: TodoListViewModelActions) -> TodoListView {
        
        return TodoListView.create(with: makeTodosListViewModel(actions: actions))
    }
    
    func makeTodosListViewModel(actions: TodoListViewModelActions) -> TodoListViewModel {
        return DefaultTodoListViewModel(todosUseCase: makeTodosUseCase(),
                                          actions: actions)
    }
    
    // MARK: - Todo Details
    func makeTodoDetailsViewController(todo: Todo) -> UIViewController {
        TodoDetailView.create(with: makeTodosDetailsViewModel(todo: todo))
    }
    
    func makeTodosDetailsViewModel(todo: Todo) -> TodoDetailViewModel {
        return DefaultTodoDetailViewModel(todo: todo)
    }
    
    
}
