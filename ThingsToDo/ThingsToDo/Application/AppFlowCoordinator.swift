//
//  AppFlowCoordinator.swift
//  ThingsToDo
//
//  Created by Muhammad Ali Maniar on 30/05/2021.
//

import UIKit

final class AppFlowCoordinator {

    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer
    
    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }

    func start() {
        // In App Flow we can check if user needs to login, if yes we would run login flow
        let todosSceneDIContainer = appDIContainer.makeTodoSceneDIContainer()
        let flow = todosSceneDIContainer.makeTodoListFlowCoordinator(navigationController: navigationController)
        flow.start()
    }
}
